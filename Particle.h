///////////////////////////////////////////////////////////////////////////////\
/// Core model: 2d geometry and model for a Particle in 2d space
#ifndef PARTICLE_H
#define PARTICLE_H
#include <iostream>

template<class T> struct v2 {
    T x;
    T y;

    v2(T _x=0, T _y=0): x(_x), y(_y) {}

    v2 operator+(const v2& r ) const {
        return v2(this->x + r.x, this->y + r.y);
    }

    v2 operator-(const v2& r ) const {
        return v2(this->x - r.x, this->y - r.y);
    }

    v2& operator+=(const v2& r) {
        x += r.x;
        y += r.y;
        return *this;
    }

    T norm2() const
    {
        return x*x + y*y;
    }

    virtual void print() const {
        std::cout << "v2: " << x << " " << y << "\n";
    }
};

template<class T, class S>
v2<T> operator*(const S & s, const v2<T> &r) 
{
    return v2<T>(r.x*s, r.y*s);
}

template<class T, class S>
v2<T> operator*(const v2<T> &r, const S & s) 
{
    return v2<T>(r.x*s, r.y*s);
}

template<class T, class S>
v2<T> operator/(const v2<T> &r, const S & s) 
{
    return v2<T>(r.x/s, r.y/s);
}

template<class T, class S>
v2<T> operator/(const S & s, const v2<T> &r) 
{
    return v2<T>(r.x/s, r.y/s);
}

typedef v2<double> real_vector_2d;

struct Particle 
{
    real_vector_2d r;
    real_vector_2d v;
    double mass;

    static bool CompareDistance(const Particle &a, const Particle & b)
    {
        return a.r.norm2() < b.r.norm2();
    }
};

#endif