///////////////////////////////////////////////////////////////////////////////
/// Utilities to interface our Planet model with:
/// - data source (loading a json via rapidjson library)
/// - screen (converting to SFML representation)

#include <fstream>
#include <vector>

#include <SFML/Graphics.hpp>

class Planet;

////////////////////////////////////////////////////////////////////////
/// data source 

std::vector<Planet> load_bodies(const char * path) ;

////////////////////////////////////////////////////////////////////////
/// UI library

struct ViewportInfo{
    unsigned int height;
    unsigned int width;
    double scale;
    ViewportInfo(unsigned int _height, unsigned int _width, double _scale): 
    height(_height), width(_width), scale(_scale) {}

    void print() const;
};

ViewportInfo calculate_viewport(unsigned int height, unsigned int width, const std::vector<Planet> & bodies);

std::pair<unsigned int, unsigned int> ui_coordinates(const ViewportInfo & info, const Planet & body);

std::vector<sf::CircleShape> ui_shapes(const ViewportInfo & info, const std::vector<Planet> & bodies);
void update_shapes(const ViewportInfo & info, std::vector<sf::CircleShape> & shapes, const std::vector<Planet> & bodies);

