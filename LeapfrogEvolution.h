#ifndef LEAPFROG_EVOLUTION_H
#define LEAPFROG_EVOLUTION_H

#include<vector>
#include "Planet.h"

const double K_G = 6.674e-11; // in m^3 kg^-1 s^-2

//Velocity Verlet - Leapfrog
struct LeapfrogEvolution
{
    double time_scale;
    LeapfrogEvolution(double _time_scale=1) : time_scale(_time_scale) {}

    void evolve(std::vector<Planet> & system, double delta);

private:
    std::vector< real_vector_2d > calculate_acceleration(std::vector<Planet> & system);
};

#endif