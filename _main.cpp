#include <SFML/Graphics.hpp>
#include <iostream>

#include "utils.h"
#include "Planet.h"
#include "LeapfrogEvolution.h"

const int HEIGHT=800;
const int WIDTH=800;


int main()
{
    // Let's trust in RVO
    auto bodies = load_bodies("planets.json");

    sf::RenderWindow window(sf::VideoMode(HEIGHT, WIDTH), "Planet simulation", sf::Style::Titlebar|sf::Style::Close);
    // SFML's default coordinate system is flipped
    sf::View view = window.getDefaultView();
    view.setSize(WIDTH, -HEIGHT); 
    window.setView(view);

    auto info = calculate_viewport(HEIGHT, WIDTH, bodies);
    auto shapes = ui_shapes(info, bodies);
    LeapfrogEvolution evolution(100 * 7 *24 * 60 *60.0); // 100 weeks per second

    sf::Clock clock;
    bool skip_first = true;
    int i=0;
    while (window.isOpen())
    {
        sf::Time elapsed = clock.restart();
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        
        if (skip_first) {
            skip_first = false;
            std::cout << "simulating\n";
        } else {
            evolution.evolve(bodies, elapsed.asSeconds());
            update_shapes(info, shapes, bodies);
        }

        for (auto shape: shapes)
        {
            window.draw(shape);
        }
        
        window.display();
    }

    return 0;
}

