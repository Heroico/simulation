cmake_minimum_required (VERSION 2.6)
set (CMAKE_CXX_STANDARD 11)
project (Simulation)
add_executable(simulation main.cpp Planet.cpp utils.cpp LeapfrogEvolution.cpp)

configure_file(planets.json planets.json COPYONLY)

include_directories(include)

find_library(SFML_GRAPHICS sfml-graphics)
find_library(SFML_WINDOW sfml-window)
find_library(SFML_SYSTEM sfml-system)
target_link_libraries(simulation ${SFML_GRAPHICS} ${SFML_WINDOW} ${SFML_SYSTEM})
#target_link_libraries(simulation sfml-graphics sfml-window sfml-system)