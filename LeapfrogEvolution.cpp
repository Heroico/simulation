#include <cmath>
#include "LeapfrogEvolution.h"

std::vector< real_vector_2d > LeapfrogEvolution::calculate_acceleration(std::vector<Planet> & system) {
    const int N = system.size();
    std::vector< real_vector_2d > a(N);
    for (int i=0; i<N; i++) {
        real_vector_2d & a_i = a.at(i);
        for (int j=0; j<N; j++) {
            if (i == j) continue;
            const auto d = system[j].r-system[i].r;
            const auto norm2_d = d.norm2();
            const auto mass_j = system[j].mass;
            a_i += K_G * mass_j / sqrt(norm2_d) * d / norm2_d;
        }
    }
    return a;
}

void LeapfrogEvolution::evolve(std::vector<Planet> & system, double delta) {
    // we are calculating "a"from last loop again...
    const auto & a_s = calculate_acceleration(system);
    for (int i=0; i<system.size(); i++) {
        Planet & planet = system[i];
        planet.v += a_s[i] * delta * time_scale/2;
        planet.r += planet.v * delta * time_scale;
    }
    const auto & a_s1 = calculate_acceleration(system);
    for (int i=0; i<system.size(); i++) {
        Planet & planet = system[i];
        planet.v += a_s1[i] * delta * time_scale/2;
    }
}