#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <utility>

#include <SFML/Graphics.hpp>

#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"

#include "utils.h"

#include "Planet.h"
#include <cmath>

rapidjson::Document load(const char * path)
{
    std::ifstream file("planets.json");
    rapidjson::IStreamWrapper fw(file);
    rapidjson::Document d;
    d.ParseStream(fw);
    return d;
}

// mass is in kg
// radius is in AU
// orbital velocity is in meters per second
// position is in AU
// color is rgb8u
//
// G is 6.674 x 10^-11 m^3 kg^-1 s^-2
const double K_AU=149597870700;
Planet load_planet(const rapidjson::Value & v, double x_sense=1.0) 
{
    Planet p;
    p.r.x = v["px"].GetDouble() * K_AU;
    p.r.y = v["py"].GetDouble() * K_AU;
    p.v.x = v["vx"].GetDouble();
    p.v.y = v["vy"].GetDouble();
    p.mass = v["mass"].GetDouble();
    p.radius = v["radius"].GetDouble() * K_AU;
    p.name = v["name"].GetString();

    const rapidjson::Value & c = v["color"];
    for (int i=0; i<3; i++) { p.color[i] = c[i].GetInt(); }
    
    return p;
}

std::vector<Planet> parse_configuration(const rapidjson::Document &configuration) 
{
    assert(configuration.HasMember("bodies"));

    const rapidjson::Value & bodies = configuration["bodies"];
    assert(bodies.IsArray());

    std::vector<Planet> r;
    for (const auto& v : bodies.GetArray()) {
        r.push_back(std::move(load_planet(v)));
    }
    
    return r;
}

std::vector<Planet> load_bodies(const char * path) 
{
    std::cout << "Reading configuration\n";
    rapidjson::Document d(std::move(load(path)));
    std::cout << "Read configuration\n";
    
    std::vector<Planet> bodies(std::move(parse_configuration(d)));
    std::cout << "Built bodies\n";
    return bodies;
}

// Transformation to screen
void ViewportInfo::print() const
{
    std::cout << "Viewport Info: height:" << height << " width:" << width << " scale:" << scale <<"\n";
}

unsigned int convert_to_ui_distance(const ViewportInfo & info, double pos)
{
    return static_cast<unsigned int>(floor(info.scale*pos)); //round() is jittery
}

unsigned int convert_to_ui(unsigned int length, double scale, double pos)
{
    double t = length/2.0 + scale*pos;
    return static_cast<unsigned int>(floor(t)); //round() is jittery
}

unsigned int convert_to_ui(const ViewportInfo & info, double pos)
{
    unsigned int length = info.height < info.width ? info.height : info.width;
    return convert_to_ui(length, info.scale, pos);
}

ViewportInfo calculate_viewport(unsigned int height, unsigned int width, const std::vector<Planet> & bodies) 
{
    unsigned int smaller = height < width ? height : width;

    auto furthest = std::max_element(bodies.begin(), bodies.end(), Particle::CompareDistance);
    double max_viewport_distance = sqrt(furthest->r.norm2()) * 1.1; //pad on the edges
    double scale = smaller/(2*max_viewport_distance);
    return ViewportInfo(height, width, scale);
}

std::pair<unsigned int, unsigned int> ui_coordinates(const ViewportInfo & info, const Planet &body)
{  
    double r = convert_to_ui_distance(info, body.radius);
    unsigned int offset = static_cast<unsigned int>(r);
    unsigned int x = convert_to_ui(info.width, info.scale, body.r.x);
    unsigned int y = convert_to_ui(info.height, info.scale, body.r.y);
    return std::pair<unsigned int, unsigned int>(x-offset, y-offset);
}

std::vector<sf::CircleShape> ui_shapes(const ViewportInfo & info, const std::vector<Planet> & bodies) 
{
    std::vector<sf::CircleShape> uis;
    for (auto body :bodies)
    {
        double screen_radius = convert_to_ui_distance(info, body.radius);
        sf::CircleShape shape(screen_radius);
        auto position = ui_coordinates(info, body);
        shape.setPosition(position.first, position.second);
        shape.setFillColor(sf::Color(body.color[0], body.color[1], body.color[2]));
        uis.push_back(shape);
    }
    return uis;
}

void update_shapes(const ViewportInfo & info, std::vector<sf::CircleShape> & shapes, const std::vector<Planet> & bodies)
{
    for (int i=0; i<shapes.size(); i++) {
        sf::CircleShape & shape = shapes[i];
        const Planet & body =  bodies[i];
        auto position = ui_coordinates(info, body);
        shape.setPosition(position.first, position.second);
    }
}

// CLUTTER
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

void print(const rapidjson::Document &d) {
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    d.Accept(writer);
    std::cout << buffer.GetString() << std::endl;
}