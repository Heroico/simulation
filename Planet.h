///////////////////////////////////////////////////////////////////////////////\
/// Core model: a planet object
#ifndef PLANET_H
#define PLANET_H
#include <string>
#include "Particle.h"

struct Planet : Particle 
{
    std::string name;
    double color[3];
    double radius;

    void print() const;
};

#endif