# Simulation

Planet simulation toy.

## Dependencies

[SFML](https://www.sfml-dev.org/) library, needs to be installed.

[Rapid Json](http://rapidjson.org/) library, included in the sources as it is header-only.

Build system based on CMake.

## Build instructions
On a unix-like system, you can do:
```
#within this repository:
mkdir build
cd build
cmake ..
make
```
On windows, if SFML is installed, CMake GUI can be used.